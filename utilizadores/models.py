from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Utilizador(User):
    telefone = PhoneNumberField(max_length=20, blank=True , null=True)
    class Meta:
        db_table = 'Utilizador'


class Administrador(Utilizador):
    
    gabinete = models.CharField(max_length=255, blank=True, null=True)
    class Meta:
        db_table = 'Administrador'



class Participante(Utilizador):
    valido = models.BooleanField(default=False)
    class Meta:
        db_table = 'Participante'



class ProfessorUniversitario(Utilizador):
    valido = models.BooleanField(default=False)
    gabinete = models.CharField(
        db_column='Gabinete', max_length=255, blank=True, null=True)
    class Meta:
        db_table = 'ProfessorUniversitario'


class Coordenador(Utilizador):
    gabinete = models.CharField(
        db_column='Gabinete', max_length=255, blank=True, null=True)

    class Meta:
        db_table = 'Coordenador'


class Colaborador(Utilizador):
    valido = models.BooleanField(default=False)
    curso = models.CharField(db_column='Curso', max_length=255)

    class Meta:
        db_table = 'Colaborador'