from django.shortcuts import render
from django.http import HttpResponse
#from .models import Utilizador
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import update_session_auth_hash
from .forms import SignUpForm


# @login_required#(login_url='/utilizadores/login/')


def consultar_utilizadores(request):
    # if request.user.is_authenticated:
    return render(request=request,
                  template_name='utilizadores/consultar_utilizadores.html',
                  context={"utilizadores": User.objects.all})
    # else
    #     return redirect(Login)




def criar_utilizador(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()

            user.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('home')
        else:
            for msg in form.error_messages:
                print(form.error_messages[msg])

            return render(request=request,
                          template_name="utilizadores/criar_utilizador.html",
                          context={"form": form})

    form = SignUpForm
    return render(request=request,
                  template_name="utilizadores/criar_utilizador.html",
                  context={"form": form})

# from .models import Inscricaoindividual, Inscricaocoletiva, Escola
# def CriarInscricaoColetiva(request: HttpRequest):
#     if request.method == 'POST':
#         participanteForm = ParticipanteForm(request.POST)
#         inscricaoForm = InscricaoColetivaForm(request.POST)
#         escolaForm = EscolaForm(request.POST)
#         if participanteForm.is_valid() and escolaForm.is_valid() and inscricaoForm.is_valid():
#             participante = participanteForm.save()
#             escola = escolaForm.save()
#             inscricao = inscricaoForm.save(commit=False)
#             inscricao.participante = participante
#             inscricao.escola = escola
#             inscricao.save()
#     else:
#         participanteForm = ParticipanteForm()
#         escolaForm = EscolaForm()
#         inscricaoForm = InscricaoColetivaForm()
#     return render(request, 'inscricoes/criar_inscricao_coletiva.html', {
#         'participante_form': participanteForm,
#         'escola_form': escolaForm,
#         'inscricao_form': inscricaoForm,
#     })


def login(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        # Redirect to a success page.
        return
    else:
        # Return an 'invalid login' error message.
        return


def logout(request):
    logout(request)
    # Redirect to a success page.


def password_change(request):
    if request.method == 'POST':
        form = PasswordChangeForm(user=request.user, data=request.POST)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
    else:
        return


def apagar_utilizador(request):
    return


def alterar_utilizador(request):
    return

# @login_required
# @transaction.atomic
# def update_profile(request):
#     if request.method == 'POST':
#         user_form = UserForm(request.POST, instance=request.user)
#         profile_form = ProfileForm(request.POST, instance=request.user.profile)
#         if user_form.is_valid() and profile_form.is_valid():
#             user_form.save()
#             profile_form.save()
#             messages.success(request, _('Your profile was successfully updated!'))
#             return redirect('settings:profile')
#         else:
#             messages.error(request, _('Please correct the error below.'))
#     else:
#         user_form = UserForm(instance=request.user)
#         profile_form = ProfileForm(instance=request.user.profile)
#     return render(request, 'profiles/profile.html', {
#         'user_form': user_form,
#         'profile_form': profile_form
#     })

# Create your views here.


def home(request):
    return render(request, "utilizadores/inicio.html")
