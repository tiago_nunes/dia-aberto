from django.urls import path, include
from .views import consultar_utilizadores
from .views import criar_utilizador
from .views import apagar_utilizador
from .views import alterar_utilizador
from .views import home
from . import views
#from django.contrib.auth.urls import views


urlpatterns = [
     path('consultarutilizadores', consultar_utilizadores,
         name='consultar-utilizadores'),
     path("criarutilizadores", views.criar_utilizador, name="criar-utilizador"), 
     path('apagarutilizador', apagar_utilizador,
         name='apagar-utilizador'),
     path('alterarutilizador', alterar_utilizador,
         name='alterar-utilizador'),
     path('accounts', include('django.contrib.auth.urls')),
     path('', home,name='home'),              
]

# accounts/login/ [name='login']
# accounts/logout/ [name='logout']
# accounts/password_change/ [name='password_change']
# accounts/password_change/done/ [name='password_change_done']
# accounts/password_reset/ [name='password_reset']
# accounts/password_reset/done/ [name='password_reset_done']
# accounts/reset/<uidb64>/<token>/ [name='password_reset_confirm']
# accounts/reset/done/ [name='password_reset_complete']