from django.forms import ModelForm
from .models import Participante,ProfessorUniversitario,Utilizador
from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.contrib.auth.models import User

class ParticipanteForm(UserCreationForm):
    class Meta:
        model = Participante
        fields = '__all__'
         

class ProfessorUniversitarioForm(UserCreationForm):
    class Meta:
        model = ProfessorUniversitario
        fields = '__all__'      

# class UserForm(forms.ModelForm):
#     class Meta:
#         model = User
#         fields = ('first_name', 'last_name', 'email','username','password',)
class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=100, help_text='Primeiro Nome')
    last_name = forms.CharField(max_length=100, help_text='Ultimo Nome')
    email = forms.EmailField(max_length=150, help_text='Email')


    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name',
'email', 'password1', 'password2',)

# class UtilizadorForm(forms.ModelForm):
#     class Meta:
#         model = Utilizador
#         fields = ('telefone',)